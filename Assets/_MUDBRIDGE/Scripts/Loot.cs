﻿using System.Collections;

/// <summary>
/// A Loot object is used in the master lootTableList and copies of those Loots are stored in the playerLootInventory list.
/// </summary>
/// <remarks>
/// Monsters have a list of loot names that refer to the actual loots in the loot table.
/// </remarks>
[System.Serializable]
public class Loot
{
    public string name = "Medpack"; // Hard-coded unique id name that is used for data tables
    public string displayName = "Piñata"; // Localizable name that is shown
    public int ammoValue = 0;
    public int healValue = 0;
    public int damageValue = 0;
	public int pointsValue = 0;
	public int costValue = 0; // The price to  at a store. -1 means it can not be puirchased
    public string ammoLootRequired = "Energy Cell";

    public Loot()
    {
        name = "Unnamed Loot";
    }
    public Loot(string lootName)
    {
        bool lootFound = false;
        if (Game.Instance != null)
        {
            foreach (Loot l in Game.Instance.lootTableList)
            {
                if (l.name == lootName)
                {
                    name = l.name;
                    ammoValue = l.ammoValue;
                    damageValue = l.damageValue;
                    pointsValue = l.pointsValue;
                    healValue = l.healValue;

                    lootFound = true;
                }
            }
            if (!lootFound)
            {
                Game.Print("Loot named " + lootName + " not found in loot table.");
            }
        }
    }//end 
}
