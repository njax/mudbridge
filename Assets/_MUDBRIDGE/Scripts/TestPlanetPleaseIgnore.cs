﻿using UnityEngine;
using System.Collections;

public class TestPlanetPleaseIgnore : MonoBehaviour
{
    public int timesConcierged = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		// if we are currently at the place with the concierge,
		if (Game.Instance.currentPlaceName == "Test Planet Angry Sign Concierge")
		{
			//Check text is printed
			if (Game.Instance.printDelayedBufferString.Length<=0)
			{
			    Game.Print("Concierge appears.");
			    if (timesConcierged > 0)
			    {
			        Game.Print("Back for more abuse?");
			    }
                Game.Print("Concierge says, \"You are not on the Test Planet Tester List. You will be returned to your ship now. This is not a test.\" The Concierge lays hands on you and forcibly returns you to your ship.");
				Game.Instance.GoToPlace("Ship");
			    Game.Instance.hitPoints -= 5;
			    timesConcierged++;
                Game.Print("The Concierge roughed you up a bit. You lost 5 hit points.");

			}
		}
		
		
		
		
		
		// then display a message about the concierge sending us back to our ship
	}
}
