﻿using UnityEngine;
using System.Collections;

public class PlanetMenu : MonoBehaviour {
	
	public static void PrintPlanetMenu()
	{
		Game.Print("You see a menu of planets you can visit.");
	}
	// Use this for initialization
	void Start () {
		foreach ( Place p in Game.Instance.places)
		{
			if (p.name == "Ship")
			{
				//Debug.Log("Adding delegate");
				//p.onPlayerEnter += PlanetMenu.PrintPlanetMenu;
			}
		}
	}

    public bool didOnce = false;
	// Update is called once per frame
    private void Update()
    {
        // if we are currently at the place with the concierge,
        if (Game.Instance.currentPlaceName == "Ship")
        {
            if (Game.Instance.printDelayedBufferString.Length <= 0 && !didOnce)
            {
                Game.Print("The ship computer screen says, \"PRESS P FOR LIST OF PLANETS.\"");
                didOnce = true;
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                int planetIndexNumber = 0;
                foreach (Place p in Game.Instance.places)
                {
                    if (p.type == "planet")
                    {
                        planetIndexNumber++;
                        Game.Print("[" + planetIndexNumber + "] " + p.name );
                    }
                }
            }
        }
    }
}
