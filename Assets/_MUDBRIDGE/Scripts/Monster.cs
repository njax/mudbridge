﻿using System.Collections.Generic;
using System.Collections;

[System.Serializable]
public class Monster
{
    public string name = "Chesthugger";
    public int hitPoints = 200;
    public int damage = 80;
    public int lootDropChance = 30;
    public List<string> lootDropNames = new List<string>();
    public bool dead = false;
	public int xpGained = 75;
	public int CurrencyMax = 50;
	public int CurrencyMin = 0;

    public Monster(string monsterName)
    {
        bool monsterFound = false;
        if (Game.Instance != null)
        {
            foreach (Monster m in Game.Instance.monsterList)
            {
                if (m.name == monsterName)
                {
                    hitPoints = m.hitPoints;
                    damage = m.damage;
                    xpGained = m.xpGained;
                    lootDropChance = m.lootDropChance;

                    monsterFound = true;
                }
            }
            if (!monsterFound)
            {
                Game.Print("Monster named " + monsterName + " not found in monsterList.");
            }
        }
    }//end monster
}